// Dear emacs, this is -*- c++ -*-
#ifndef DF_FILE_SESSION_H
#define DF_FILE_SESSION_H

#include <mutex>

#include "dfinterface/dfinterface.h"
#include "eformat/compression.h"


namespace EventStorage{
  class DataReader;
  class DataWriter;
  struct run_parameters_record;
}

namespace HLTMP{
  /*! File based implementation of dfinterface::Session
   *
   * It doesn't require a separate data source such as Dcm.
   * It can read/write ATLAS bytestream files, read ROB fragments from
   * the input file and write results sent by HLT into output file.
   *
   * It can only be used while running with 1 fork/thread, as each
   * instance will create a new file reader/writer.
   */
  class DFFileSession:public daq::dfinterface::Session{

  public:
    DFFileSession(const boost::property_tree::ptree &args);
    /*! \brief Opens the Session.
     *
     *  \param[in] clientName Name of the application opening the Session. The application name
     *      must be suitable to report a misbehaving application to the Run Control infrastructure.
     *
     *  \exception boost::system::system_error is thrown if the Session can not be created due to
     *      communication issues with the service, e.g. the service is unreachable or does not
     *      respond 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual void open(const std::string& clientName);

    /*! \brief Determines whether the Session is open.
     */
    virtual bool isOpen() const;

    /*! \brief Closes the Session.
     */
    virtual void close();

    /*! \brief Returns a pointer to the next \ref Event object to be processed.
     *
     *  Blocks indefinitely while waiting for new events.
     *
     *  \exception NoMoreEvents is thrown if there are no more events to be processed.
     *  \exception boost::system::system_error is thrown in case of network communication issues
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual std::unique_ptr<daq::dfinterface::Event> getNext();

    /*! \brief Returns a pointer to the next \ref Event object to be processed.
     *
     *  Blocks until an event can be obtained, or the specified time is reached.
     *
     *  \exception OperationTimedOut is thrown if an event could not be obtained before \c absTime.
     *  \exception NoMoreEvents is thrown if there are no more events to be processed.
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual std::unique_ptr<daq::dfinterface::Event> tryGetNextUntil(
						   const std::chrono::steady_clock::time_point& absTime);

    /*! \brief Returns a pointer to the next \ref Event object to be processed.
     *
     *  Equivalent to:
     *  \code
     *  tryGetNextUntil(std::chrono::steady_clock::now() + relTime)
     *  \endcode
     */
    virtual std::unique_ptr<daq::dfinterface::Event> tryGetNextFor(
						 const std::chrono::steady_clock::duration& relTime);

    /*! \brief Marks the event as accepted by the High-Level Trigger.
     *
     *  \param[in] event An \ref Event object obtained from getNext(), tryGetNextUntil() or
     *      tryGetNextFor()
     *  \param[in] hltr Points to serialized read::FullEventFragment, it should contain High Level
     *      trigger information, streamTags, pscErrors and HLT fragments
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     *
     */
    virtual void accept(std::unique_ptr<daq::dfinterface::Event> event,
			std::unique_ptr<uint32_t[]> hltr);

    /*! \brief Marks the event as rejected by the High-Level Trigger.
     *
     *  \param[in] event An \ref Event object obtained from getNextEvent(),
     *      tryGetNextEventUntil() or  tryGetNextEventFor()
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual void reject(std::unique_ptr<daq::dfinterface::Event> event);

    virtual ~DFFileSession();

  private:
    bool nextFile();
    std::unique_ptr<uint32_t[]> getNextEvent();
    bool skipEvents(uint num);
    std::vector<std::string> m_fileNames;
    std::unique_ptr<EventStorage::DataReader> m_currReader;
    std::unique_ptr<EventStorage::DataWriter> m_writer;
    std::string m_outFileName;
    bool m_loopFiles;
    int m_stride;
    int m_offset;
    int m_numEventsInCurrFile;
    int m_currFile;
    int m_currEventInFile;
    int m_start;
    int m_nMaxEvents;
    int m_nEvents;
    unsigned int m_nSkip;    // Number of events to skip
    eformat::Compression m_comp;
    unsigned int m_compLevel;
    std::vector<uint32_t> m_extraL1Robs;  // List of extra ROBs that should be retrieved in L1 results

  };
}
#endif
