#ifndef HLTMPPU_SHAREDMEMORYUTILS_H_
#define HLTMPPU_SHAREDMEMORYUTILS_H_

#include <memory>
#include <vector>
#include <string>
#include <sstream>

#include "ers/ers.h"

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>

using namespace boost::interprocess;

// Define an STL compatible allocator of ints that allocates from the managed_shared_memory.
// This allocator will allow placing containers in the segment
using  ShmemAllocator = allocator<uint32_t, managed_shared_memory::segment_manager>;

// Alias a vector that uses the previous STL-like allocator so that allocates
// its values from the segment
using shared_vector = vector<uint32_t, ShmemAllocator>;

namespace HLTMP {

/*! Shared Vector Wrapper Class
 *
 * managed_shared_memory and boost::interprocess::vector make it possible to set the
 * shared memory size at run-time instead of compile time. This class managed the
 * shared memory segment, and the vector within the segment. It can be constructed
 * creating a new area or opening an existing memory area.
 *
*/
class SharedVectorHandler {
 private:
  bool m_open_only;  // If true, don't destroy the shared memory and vector in the end
  std::string m_shared_name;  // Name of the shared memory segment
  managed_shared_memory m_segment;
  shared_vector* m_vector;  // Pointer to shared vector

 public:
  //! Create a new instance with creating a new shared memory segment
  //!
  //! Remove any existing shared memory with same name before creating it
  //! \param shared_memory_name Name of the shared memory segment
  //! \param size Size of the shared memory segment in bytes
  SharedVectorHandler(std::string shared_memory_name, size_t size):m_shared_name(shared_memory_name) {
    ERS_DEBUG(3, "Creating SharedVectorHandler with name " << m_shared_name << ", size " << size);
    m_open_only = false;
    shared_memory_object::remove(m_shared_name.c_str());
    m_segment = managed_shared_memory(create_only, m_shared_name.c_str(), size);

    // Initialize shared memory STL-compatible allocator
    const ShmemAllocator alloc_inst(m_segment.get_segment_manager());
    m_vector = m_segment.construct<shared_vector>("Vector")(alloc_inst);
    ERS_DEBUG(5, "Shared memory size(bytes): " << getSize() << ", free memory size(bytes): " << getFreeSize());
  }

  //! Opening an existing shared memory segment and find the vector
  //!
  //! Instances created with this constructor will not destroy the shared
  //! memory segment at destructor
  //! \param shared_memory_name Name of the shared memory segment
  explicit SharedVectorHandler(std::string shared_memory_name):m_shared_name(shared_memory_name) {
    m_open_only = true;  // This prevents shared memory from being destroyed
    ERS_DEBUG(3, "Opening SharedVectorHandler with name " << m_shared_name);
    m_segment = managed_shared_memory(open_only, shared_memory_name.c_str());

    // Initialize shared memory STL-compatible allocator
    const ShmemAllocator alloc_inst(m_segment.get_segment_manager());

    m_vector = m_segment.find<shared_vector>("Vector").first;
  }

  //! Destructor, shared memory segment and vector will be destroyed if m_open_only is false
  ~SharedVectorHandler() {
    ERS_DEBUG(6, "dtor");
    if (!m_open_only) {
      ERS_DEBUG(5, "Destroying SharedVectorHandler with name " << m_shared_name);
      m_segment.destroy<shared_vector>("Vector");
      shared_memory_object::remove(m_shared_name.c_str());
    }
  }

  //! Copy array content to shared vector
  //!
  //! This function will throw boost::interprocess::bad_alloc if array doesn't fit in the free memory
  //!
  //! \param blob pointer to first element of array
  //! \param size size of array
  void assign(uint32_t *blob, size_t size) {
    ERS_DEBUG(4, "Assigning " << size*sizeof(uint32_t) << " bytes, maximum allowed: " << getFreeSize());
    // TODO(cyildiz): How about growing the memory segment in case of size mismatch?
    try {
      m_vector->assign(blob, blob + size);
    } catch (boost::interprocess::bad_alloc & e) {
      std::stringstream stream;
      stream << "Problem assigning " << size*sizeof(uint32_t) << " bytes, maximum allowed: " << getFreeSize()
             << ". Try increasing shared memory size";
      ERS_LOG(stream.str());
      ers::warning(ers::Message(ERS_HERE, stream.str()));
      throw;
    }
  }

  shared_vector* getVectorPtr() { return m_vector;}

  std::string tostring() {
    auto size = m_vector->size();
    if (size == 0) {
      return "Vector empty";
    }
    std::ostringstream oss;
    oss << "  vec[0]  : 0x" << std::hex << m_vector->at(0) << " = " << std::dec << m_vector->at(0) << "\n"
        << "  vec[1]  : 0x" << std::hex << m_vector->at(1) << " = " << std::dec << m_vector->at(1) << "\n"
        << "  vec[2]  : 0x" << std::hex << m_vector->at(2) << " = " << std::dec << m_vector->at(2) << "\n"
        << "  vec[N-2]: 0x" << std::hex << m_vector->at(size-3) << " = " << std::dec << m_vector->at(size-3) << "\n"
        << "  vec[N-1]: 0x" << std::hex << m_vector->at(size-2) << " = " << std::dec << m_vector->at(size-2) << "\n"
        << "  vec[N]  : 0x" << std::hex << m_vector->at(size-1) << " = " << std::dec << m_vector->at(size-1) << "\n";
    return oss.str();
  }

  //! Return the size of shared memory segment in bytes
  size_t getSize() { return m_segment.get_size();}

  //! Return the size of free area of shared memory segment in bytes
  size_t getFreeSize() { return m_segment.get_free_memory();}
};

}  // namespace HLTMP

#endif  // HLTMPPU_SHAREDMEMORYUTILS_H_
