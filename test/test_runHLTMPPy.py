#!/usr/bin/env python

# Script to run different runHLTMPPU configurations
# It's not integrated in a testing framework
# - It needs to be run by user
# - There is no simple method to define success. One just needs to make sure none of the configurations have any serious error in "err" file
# This script will create one folder for each test.
# To run it in the same main folder, one needs to remove all previously created folders
# All configurations use pudummy, thus setting up tdaq environment is enough.
#
# Once all runs are finished, one can inspect logs using following commands on the shell
# for i in *; do echo "** TEST: " $i; ls $i/chlog/*out |echo "-> Number of child Log files:" $(wc -l) ; grep -i "exited with\|all children exited" $i/log ; echo "--" ; grep "events:   " $i/chlog/*out ; printf "\n******************\n\n" ;done
# grep -v "gsl\|GSL" */err


from __future__ import print_function
from builtins import object
import os
import subprocess
import shutil

datafile="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data18_13TeV.00364485.physics_EnhancedBias.merge.RAW._lb0705._SFO-1._0001.1"
ros2robmap="{'ROS-IBL-B-00':[1310816,1310817,],'ROS-LAR-EMBA-01':[4259844,4259845],'ROS-SCT-ECC-01':[2359296,2359297,2359298,2359299]}"


class TestRunner(object):
  """docstring for TestRunner"""
  def __init__(self, testname):
    super(TestRunner, self).__init__()
    self.testname = testname
    self.command = ""
    self.transitions = ""   # Transitions to be used in interactive mode
                            # It should be a string like: "f\nf\nf\nb\nb\nb\ne\n"
    self.extra_command = ""  # Any extra shell command to run before runHLTMPPy
    self.trans_fname = "transitions.txt"  # transition file name for interactive mode

  def prepare(self):
    try:
      os.mkdir(self.testname)
    except Exception:
      shutil.rmtree(self.testname)
      os.mkdir(self.testname)

    os.chdir(self.testname)

    if self.transitions:
      self.trans_fname = 'transitions.txt'
      with open(self.trans_fname, 'w') as f:
        f.write(self.transitions)

    if self.extra_command:
      print("Running extra command: {}".format(self.extra_command))
      os.system(self.extra_command)

  def run(self):
    self.prepare()
    print(self.command)
    stdin = None
    if self.transitions:
      stdin = open(self.trans_fname) 

    with open("command", 'w') as f:
      f.write(self.command + "\n")
    proc = subprocess.Popen(self.command.split(), stdout=open("log","w"), stderr=open("err","w"), stdin=stdin)
    os.chdir("..")
    return proc

tests = {}

testname = "onefork"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -N {tn} -L {tn} --num-forks 1 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname = "onefork_onethread_DFFile"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py --ros2robs {rr} -l chlog  dffileds --dslibrary DFFileBackend --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -N {tn} -L {tn} --num-forks 1 --num-slots 1 --num-threads 1 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname="24fork"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 96 --outFile out{tn}.data pudummy HLTMPPU -N {tn} -L {tn} --num-forks 24 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t


testname="extral1robs"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py -I --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data --extraL1Robs 1310816 --extraL1Robs 1310817 pudummy HLTMPPU -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

# This test should create warnings that hlt results size is too large for shared memory
testname="hltresultSize"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py -I --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU --hltresultSizeMb 1 -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname="infrastructure"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py -I --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname="interactive"
t = TestRunner(testname)
t.transitions = "f\nf\nf\nb\nb\nb\ne\n"
t.command = 'runHLTMPPy.py --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -i -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname="interactiveSTOP_START"
t = TestRunner(testname)
t.transitions = "f\nf\nf\nb\nf\nb\nb\nb\ne\n"
t.command = 'runHLTMPPy.py --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -i -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname="interactiveUNCONFIG_CONFIG"
t = TestRunner(testname)
t.transitions = "f\nf\nf\nb\nb\nb\nf\nf\nf\nb\nb\nb\ne\n"
t.command = 'runHLTMPPy.py --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -i -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname)
tests[testname] = t

testname="ros2rob_fromfile"
t = TestRunner(testname)
ros2robfile="ros2robs.txt"
t.extra_command = "echo \"{rr}\" > {rrfile}".format(rr=ros2robmap, rrfile=ros2robfile)
t.command = 'runHLTMPPy.py --ros2robs {rrfile} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data pudummy HLTMPPU -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rrfile=ros2robfile, df=datafile, tn=testname)
tests[testname] = t

testname="cfgdict_3forks_24ev"
cfgdict="{'HLTMPPU':{'num_forks':3},'datasource':{'numEvents':24}}"
t = TestRunner(testname)
t.command = 'runHLTMPPy.py --cfgdict {cfgdict} -I --ros2robs {rr} -l chlog  dffileds --file {df} --numEvents 16 --outFile out{tn}.data --extraL1Robs 1310816 --extraL1Robs 1310817 pudummy HLTMPPU -N {tn} -L {tn} --num-forks 2 --num-slots 8 --num-threads 8 '.format(
                rr=ros2robmap, df=datafile, tn=testname, cfgdict=cfgdict)
tests[testname] = t

procs = [tests[name].run() for name in tests]
for p in procs:
  p.wait()

