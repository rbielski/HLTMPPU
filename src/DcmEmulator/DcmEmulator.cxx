
#include "HLTMPPU/DcmEmulator/DcmEmulator.h"
#include "HLTMPPU/eformat_utils.h"

#include <cstring>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <chrono>
#include <sstream>
#include <string>
#include <future>
#include <list>

#include <boost/foreach.hpp>
#include "boost/property_tree/json_parser.hpp"
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/shared_memory_object.hpp>


#include "eformat/eformat.h"
#include "eformat/index.h"
#include "eformat/write/eformat.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/FullEventFragmentNoTemplates.h"

using namespace boost::interprocess;

HLTMP::DcmEmulator::DcmEmulator(const boost::property_tree::ptree & cargs):
    m_ros_hit_stats_accept(cargs), m_ros_hit_stats_reject(cargs) {
  m_nworkers = cargs.get("Configuration.HLTMPPUApplication.numForks", 1);
  m_nevents_per_worker = cargs.get("Configuration.HLTMPPUApplication.numberOfEventSlots", 1);

  // Dcm Emulator is run if we use HLTDFFileBackend and DFDcmEmuBackend as the library
  m_args = cargs.get_child("Configuration.HLTMPPUApplication.DataSource.HLTDFFileBackend");
  m_comp = eformat::UNCOMPRESSED;
  m_compLevel = 0;
  try {
    auto of = m_args.get_child("compressionFormat");
    std::string ctype(of.data());
    if (ctype == "ZLIB") {
      m_comp = eformat::ZLIB;
      m_compLevel = m_args.get("compressionLevel", 2);
    }
  } catch(boost::property_tree::ptree_bad_path &ex) {
    ERS_DEBUG(1, "Compression is not specified");
    ERS_LOG("Failed to get Compression information");
  }

  m_ros_hit_stats_basename = m_args.get("rosHitStatFileName", "ros_hitstats");

  size_t max_hltres_size_MB = cargs.get("Configuration.HLTMPPUApplication.maximumHltResultMb", 10);  // Default value is 10MB
  m_max_hltres_size = max_hltres_size_MB * 1024 * 1024;

  // Set base name as the TDAQ_APPLICATION_NAME, as this will be called in runHLTMPPy
  char * base_name = getenv("TDAQ_APPLICATION_NAME");
  if (base_name==nullptr) {
    m_base_name="DummyApplication";
    ERS_LOG("TDAQ_APPLICATION_NAME not in environment, setting a dummy name: " << m_base_name);
  } else {
    m_base_name = std::string(base_name);
    ERS_LOG("Setting name using TDAQ_APPLICATION_NAME: " << m_base_name);
  }
  m_nomoreevents = false;
  m_readEvents = 0;
  m_stop = false;

  // Clean up shared memory from previous sessions, in case not cleaned!
  ERS_DEBUG(1, "Removing any dangling shared memory with same name");
  for (size_t i = 1; i < m_nworkers+1 ; i++) {
    for (size_t j = 0; j < m_nevents_per_worker ; j++) {
      std::stringstream ss;
      ss << m_base_name << "-" << std::setfill('0') << std::setw(2) << i << "-" << std::setfill('0') << std::setw(2) << j;
      auto name = ss.str();
      shared_memory_object::remove(name.c_str());
    }
  }
}

HLTMP::DcmEmulator::~DcmEmulator() {
  ERS_DEBUG(1, "Destroying DcmEmulator");
}

std::unique_ptr<uint32_t[]> HLTMP::DcmEmulator::getNextEvent() {
  std::unique_ptr<uint32_t[]> blob;

  // Return immediately if no more events or stopRun is called
  if (m_nomoreevents || m_stop) {
    return blob;
  }

  try {
     blob = m_file_rw->getNextEvent();
     m_readEvents++;
     ERS_DEBUG(2, "Event number: " << m_readEvents);
  } catch (HLTMP::NoMoreEventsInFile & ex) {
    ERS_LOG("No more events!");
    m_nomoreevents = 1;
  }
  return blob;
}

void HLTMP::DcmEmulator::configure() {
}

void HLTMP::DcmEmulator::connect() {
}

void HLTMP::DcmEmulator::prepareForRun(const boost::property_tree::ptree & pargs) {
  ERS_DEBUG(1, "Enter");

  // Create a handler for each session
  // ApplicationName-XX-YY  (XX: worker starting from 1, YY: session starting from 0)
  for (size_t i = 1; i < m_nworkers+1 ; i++) {
    for (size_t j = 0; j < m_nevents_per_worker ; j++) {
      std::stringstream ss;
      ss << m_base_name << "-" << std::setfill('0') << std::setw(2) << i << "-" << std::setfill('0') << std::setw(2) << j;
      auto name = ss.str();
      m_sessions.emplace(name, name);
    }
  }

  m_stop = false;
  m_nomoreevents = false;

  // Initialize file reader writer
  m_args.add_child("RunParams", pargs.get_child("RunParams"));
  m_file_rw = std::make_unique<FileReaderWriter>(m_args);

  for (auto &[session_name, handler] : m_sessions) {
    m_event_futures[session_name];  // Create an empty list for each session
    m_session_threads.emplace_back(&DcmEmulator::listenSession, this, session_name);
  }

  ERS_DEBUG(1, "Exit");
}

void HLTMP::DcmEmulator::stopRun() {
  ERS_DEBUG(1, "Enter");
  m_stop = true;

  ERS_LOG("Notifying all event threads that are waiting for eventDone calls");
  for (auto &[gid,handler] : m_event_handlers) {
    event_info * ev = handler.m_event_info;
    ev->cond_hltresult.notify_one();
  }

  {
    std::lock_guard<std::mutex> lock_next(m_eventfuture_mutex);
    ERS_LOG("Waiting for " << m_event_futures.size() << " event threads to join");
    for (auto & [sname,list] : m_event_futures) {
      for (auto & future : list) {
        future.wait();
      }
    }
    m_event_futures.clear();
  }

  m_event_handlers.clear();

  // Notify session threads that are waiting for event requests
  ERS_LOG("Notifying all session threads that are waiting for event requests");
  for (auto &[session_name, handler] : m_sessions) {
    handler.m_session_info->cond_next.notify_one();
  }
  ERS_LOG("Waiting for " << m_session_threads.size() << " session threads to join");
  for (auto && thread : m_session_threads) {
    thread.join();
  }
  ERS_LOG("Events processed: " << m_readEvents);
  // Delete all DcmSessionHandler objects
  m_sessions.clear();
  m_session_threads.clear();

  // Delete file reader, this will finish writing and close the file
  m_file_rw.reset();

  // Following part won't do anything, if ROS2ROB map is empty
  if (m_ros_hit_stats_reject.isEnabled() || m_ros_hit_stats_accept.isEnabled()) {
    m_ros_hit_stats_reject.calculate();
    m_ros_hit_stats_accept.calculate();

    std::string filename = m_ros_hit_stats_basename + "_reject.txt";
    std::ofstream ostrm_r(filename);
    m_ros_hit_stats_reject.print(ostrm_r);

    filename = m_ros_hit_stats_basename + "_accept.txt";
    std::ofstream ostrm_a(filename);
    m_ros_hit_stats_accept.print(ostrm_a);
  }
  ERS_DEBUG(1, "Exit");
}

void HLTMP::DcmEmulator::disconnect() {
}

void HLTMP::DcmEmulator::unconfigure() {
}

void HLTMP::DcmEmulator::cleanup_futures(std::string session_name) {
  std::lock_guard<std::mutex> lock_next(m_eventfuture_mutex);
  std::future_status status;
  auto & list = m_event_futures[session_name];
  ERS_DEBUG(3, "Session: " << session_name << ", Number of futures: " << list.size());
  for (auto it = list.begin(); it != list.end();) {
    status = it->wait_for(std::chrono::microseconds(1));
    if (status == std::future_status::ready) { // This means function call completed and we can remove it
      ERS_DEBUG(5, "Future ready, erasing from list");
      it = list.erase(it);  // Automatically does it++
    } else if (status == std::future_status::timeout) {
      ERS_DEBUG(5, "Future timeout, future not ready,");
      it++;
    } else if (status == std::future_status::deferred) {
      ERS_DEBUG(5, "Future deferred, this should not have happened!");
      it++;
    }
  }
}

void HLTMP::DcmEmulator::listenSession(std::string session_name) {
  ERS_DEBUG(1, "Enter listener for " << session_name);
  do {
    session_info * data = m_sessions.at(session_name).m_session_info;

    ERS_DEBUG(2, session_name << " trying to lock mutex");
    scoped_lock<interprocess_mutex> lock(data->mutex);
    ERS_DEBUG(2, session_name << " locked mutex");

    ERS_DEBUG(2, session_name << " waiting for next event request");
    data->dcm_running = true;
    data->cond_next.wait(lock);
    ERS_DEBUG(2, session_name << " wait ended");

    {
      // Lock event getter, other workers can be asking for events at the same time
      ERS_DEBUG(2, session_name << " trying to lock read_mutex");
      std::lock_guard<std::mutex> lock_next(m_readmutex);
      ERS_DEBUG(2, session_name << " locked read_mutex");

      // Read event into memory
      auto blob = getNextEvent();

      if (m_nomoreevents || m_stop) {
        data->nomoreevents = true;
        data->cond_ready.notify_one();
        continue;
      }

      // Build FullEventFragment to get global_id
      eformat::read::FullEventFragment event(blob.get());
      auto gid = event.global_id();

      // Get size of the full event in words
      auto fullev_size_words = blob.get()[1];
      // Convert to bytes, and add 1024 bytes, because not all memory in the segment is allocatable to a vector
      // Boost documentation isn't clear about this, but emprically it's observed the free memory is ~320 bytes
      // less then the size of the shared memory segment
      auto fullev_size_bytes = fullev_size_words * sizeof(uint32_t) + 1024;

      //Preparing the handler name
      std::string handler_name = session_name + "-" + std::to_string(gid);

      // Create the event handler
      {
        std::lock_guard<std::mutex> lock_next(m_eventhandler_mutex);
        m_event_handlers.try_emplace(handler_name, std::move(blob), handler_name, fullev_size_bytes, m_max_hltres_size);
      }
      ERS_DEBUG(2, session_name << "-" << gid << " New event sent");

      // Set the name of the shared memory
      std::snprintf(data->shared_name, sizeof(data->shared_name), "%s-%lu", session_name.c_str(), gid);

      cleanup_futures(session_name);
      {
        std::lock_guard<std::mutex> lock_next(m_eventfuture_mutex);

        // Start waitForEventDone asyncronously, put it's future in the container for cleaup later
        // std::async is preferred instead of std::thread. Because the future can indicate if the function exited
        // and this helps periodical cleanup
        m_event_futures[session_name].push_back(std::async(std::launch::async, &DcmEmulator::waitForEventDone, this, handler_name));
      }

      // Notify the process that the event is ready
      data->cond_ready.notify_one();
    }
  } while (!m_nomoreevents && !m_stop);
  ERS_DEBUG(1, "Exit listener for " << session_name);
}

void HLTMP::DcmEmulator::waitForEventDone(const std::string& handler_name) {
  ERS_DEBUG(2, handler_name << " Enter event listener");

  event_info * data;
  uint32_t* fulleventptr;
  uint32_t* hltresultptr;
  {
    std::lock_guard<std::mutex> lock_next(m_eventhandler_mutex);
    data = m_event_handlers.at(handler_name).m_event_info;
  }

  { // This has to be a scope, because we'll delete shared memory afterwards
    ERS_DEBUG(3, handler_name << " Locking mutex");
    scoped_lock<interprocess_mutex> lock(data->mutex);
    ERS_DEBUG(3, handler_name << " Locked mutex, waiting");

    // What happens if worker crashes during processing?
    // Normally HLTSV would notify DCM?
    // TODO(cyildiz): Find a way to make Dcm aware if a session disconnected
    data->cond_hltresult.wait(lock);
    ERS_DEBUG(3, handler_name << " HLTResult received");

    if (data->accept) {
      fulleventptr = m_event_handlers.at(handler_name).m_fullevent->getVectorPtr()->data();
      hltresultptr = m_event_handlers.at(handler_name).m_hltresult->getVectorPtr()->data();

      ERS_DEBUG(2, handler_name << " Accepting event");
      std::vector<uint32_t> finalEvent;
      finalEvent = merge_hltresult_with_input_event(hltresultptr, fulleventptr, m_comp, m_compLevel);

      auto finalSize = finalEvent.size();
      auto sizeInBytes = finalSize*4;  // finalSize is in words(4 byte chunks), putData argument is bytes
      ERS_DEBUG(2, "Writing " << sizeInBytes << " bytes ");
      {
        std::lock_guard<std::mutex> lock_next(m_writemutex);  // Protect consecutive write actions
        m_file_rw->writeEvent(sizeInBytes, finalEvent.data());
      }
      std::lock_guard<std::mutex> lock(m_statmutex);  // Protect updating of ros stats
      m_ros_hit_stats_accept.add(data->ros_stats);
    }  else {
      ERS_DEBUG(2, handler_name << " Rejecting event");
      std::lock_guard<std::mutex> lock(m_statmutex);  // Protect updating of ros stats
      m_ros_hit_stats_reject.add(data->ros_stats);
    }
    ERS_DEBUG(4, "ROS Stats for " << handler_name << " : " << data->ros_stats);
  }
  // After this the DcmEventHandler can be erased
  ERS_DEBUG(4, handler_name << " Erasing event handler");
  {
    std::lock_guard<std::mutex> lock_next(m_eventhandler_mutex);
    m_event_handlers.erase(handler_name);
  }
  ERS_DEBUG(2, handler_name << " Exit event listener");
}

HLTMP::DcmSessionHandler::DcmSessionHandler(const std::string name): m_name(name) {
  ERS_LOG("DCM session handler for: " << name);

  m_smh = std::make_unique<SharedMemoryHandler>(name, sizeof(session_info));

  m_session_info = new (m_smh->getAddress()) session_info;
}

HLTMP::DcmEventHandler::DcmEventHandler(std::unique_ptr<uint32_t[]> blob, std::string base_shared_name, size_t max_fullev_size, size_t max_hltres_size) {
  // Get global_id of event
  eformat::read::FullEventFragment event(blob.get());
  m_gid = event.global_id();

  ERS_DEBUG(2, "Creating Event Handler with base name: " << base_shared_name
               << ", gid: " << m_gid);

  auto ptr = blob.get();
  uint32_t nwords = ptr[1];  // Size of FullEventFragment in words

  ERS_DEBUG(4, "Before copying: \n"
            << "nFirst 3: " << std::hex << ptr[0] << "\n"
            << "         "  << std::hex << ptr[1] << "\n"
            << "         "  << std::hex << ptr[2] << "\n"
            << "size:    "  << std::dec << nwords << "\n"
            << "Last 3:  "  << std::hex << ptr[nwords-3] << "\n"
            << "         "  << std::hex << ptr[nwords-2] << "\n"
            << "         "  << std::hex << ptr[nwords-1] << "\n"
            << std::dec << "\n");

  m_shared_name = base_shared_name; 
  m_smh = std::make_unique<SharedMemoryHandler>(m_shared_name, sizeof(event_info));

  m_fullevent = std::make_unique<SharedVectorHandler>(m_shared_name + "_fullevent", max_fullev_size);
  m_hltresult = std::make_unique<SharedVectorHandler>(m_shared_name + "_hltresult", max_hltres_size);

  m_event_info = new (m_smh->getAddress()) event_info;

  // Copy event into shared vector, after this blob will be destroyed
  m_fullevent->assign(blob.get(), nwords);
  ERS_DEBUG(4, "after copying: \n" << m_fullevent->tostring());
}

HLTMP::SharedMemoryHandler::SharedMemoryHandler(const std::string name, const size_t mem_size):m_name(name) {
  // Erase previous shared memory with same name, if exists
  ERS_DEBUG(4, "Erasing previous shared memory: " << m_name);
  shared_memory_object::remove(m_name.c_str());

  ERS_DEBUG(3, "Creating shared memory: " << m_name << ", size(bytes): " << mem_size);
  m_shared_memory = shared_memory_object(create_only, m_name.c_str(), read_write);

  m_shared_memory.truncate(mem_size);

  m_region = mapped_region(m_shared_memory, read_write);
}

HLTMP::SharedMemoryHandler::~SharedMemoryHandler() {
  ERS_DEBUG(3, "Destroying SharedMemoryHandler instance: " << m_name);
  shared_memory_object::remove(m_name.c_str());
}

HLTMP::ROSHitStats::ROSHitStats(const boost::property_tree::ptree & config) {
  auto ros2robtree = config.get_child("Configuration.ROS2ROBS");
  using ptree=boost::property_tree::ptree;
  BOOST_FOREACH(const ptree::value_type &rostree, ros2robtree) {
    std::string rosname = rostree.first;
    ptree robs = rostree.second;
    size_t nrobs = 0;
    BOOST_FOREACH([[maybe_unused]]const ptree::value_type &rob, robs) {
        nrobs++;
    }
    m_nrobs[rosname] = nrobs;

    // Initialize map Hits object for each ROS
    m_total[rosname] = std::move(ROSFractions());
    m_evbuild[rosname] = std::move(ROSFractions());
    m_noevbuild[rosname] = std::move(ROSFractions());
  }
  m_nevents = 0;
  if (m_nrobs.empty()) {
    m_enabled = false;
  } else {
    m_enabled = true;
  }
}

void HLTMP::ROSHitStats::add(std::string event_stats) {
  if (!isEnabled()) {
    return;
  }
  std::stringstream stream;
  stream << event_stats;
  boost::property_tree::ptree stats;
  try {
    boost::property_tree::read_json(stream, stats);

    // Add number of ros hits/rob hits and total size.
    // Normalization with number of robs and events will
    // be done at the end
    for (auto & [ros,nrobs] : m_nrobs) {
      m_evbuild[ros].ros = m_evbuild[ros].ros + stats.get<double>(ros + ".evbuild.ros");
      m_evbuild[ros].rob = m_evbuild[ros].rob + stats.get<double>(ros + ".evbuild.rob");
      m_evbuild[ros].size = m_evbuild[ros].size + stats.get<double>(ros + ".evbuild.size");
      m_noevbuild[ros].ros = m_noevbuild[ros].ros + stats.get<double>(ros + ".noevbuild.ros");
      m_noevbuild[ros].rob = m_noevbuild[ros].rob + stats.get<double>(ros + ".noevbuild.rob");
      m_noevbuild[ros].size = m_noevbuild[ros].size + stats.get<double>(ros + ".noevbuild.size");
    }
    m_nevents++;
  } catch (boost::property_tree::json_parser::json_parser_error & ex) {
    ERS_DEBUG(2, "Problem reading json string due to: " << ex.what()
                 << "\nString content: " << event_stats);
  } catch (boost::property_tree::ptree_bad_path & ex) {
    ERS_DEBUG(2, "Problem getting one or more elements from json string due to: " << ex.what()
                 << "\nString content: " << event_stats);
  }
}

void HLTMP::ROSHitStats::calculate() {
  if (!isEnabled()) {
    return;
  }
  if (!m_nevents) {  // Nothing to calculate
    return;
  }
  for (auto & [ros,nrobs] : m_nrobs) {
    auto & fraceb = m_evbuild[ros];
    auto & fracnoeb = m_noevbuild[ros];
    auto & fractot = m_total[ros];

    auto normalize = [](ROSFractions & frac, size_t m_nevents, size_t nrobs) {
      if ( frac.ros > 0) {
       frac.rob = frac.rob/frac.ros/nrobs;    
       frac.size = frac.size/frac.ros;
      }
      frac.ros = frac.ros/m_nevents; 
    };

    normalize(fraceb,m_nevents,nrobs);
    normalize(fracnoeb,m_nevents,nrobs);

    fractot.ros = fraceb.ros + fracnoeb.ros;
    fractot.rob = fraceb.rob + fracnoeb.rob;
    fractot.size = fraceb.size + fracnoeb.size;
  }
}

void HLTMP::ROSHitStats::print(std::ostream & out) {
  if (!isEnabled()) {
    return;
  }
  out << "Number of Events: " << m_nevents << "\n";
  out << "                            | total                               | no evt. bld.                        | evt. bld.                           |" << "\n"
      << "ROS                | # ROBs | Hits/Evt    ROB frac/Evt  bytes/Evt | Hits/Evt    ROB frac/Evt  bytes/Evt | Hits/Evt    ROB frac/Evt  bytes/Evt |" << "\n";
  for (auto & [ros,nrobs] : m_nrobs) {
    auto & fraceb = m_evbuild[ros];
    auto & fracnoeb = m_noevbuild[ros];
    auto & fractot = m_total[ros];

    auto printone = [](ROSFractions & frac) {
      std::stringstream ss;
      ss << std::setprecision(4) << std::fixed
         << std::setw(9) << frac.ros  << " , "
         << std::setw(9) << frac.rob  << " , "
         << std::setw(11) << frac.size  << " | ";
      return ss.str();
    };

    out << std::setw(18) << ros << " | " << std::setw(6) << nrobs << " | "
              << printone(fractot) << printone(fracnoeb) << printone(fraceb) << "\n";
  }
}
