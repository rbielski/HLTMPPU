// Dear emacs, this is -*- c++ -*-

/**
 * @file HLTMPPU_main.cxx
 * @author <a href="mailto:sami.kama _at_ cern.ch">Sami Kama</a>
 * @brief Loads the HLTInterface library  and conects it to HLTRC 
 */

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <ers/ers.h>
#include <boost/program_options.hpp>
#include "ipc/partition.h"
#include "ipc/core.h"
#include "HLTRC/HLTReceiver.h"
#include "HLTRC/Exceptions.h"
#include "hltinterface/HLTInterface.h"
#include "HLTMPPU/Issues.h"
#include "HLTMPPU/HLTMPPU_utils.h"
#include <dlfcn.h>


std::unique_ptr<daq::rc::hlt::HLTReceiver> hltRec;
using creator = hltinterface::HLTInterface* (*)(void);
using destroyer = void (*)(hltinterface::HLTInterface*);

extern char **environ;

void handler(int signal) {
  (void) signal;
  std::cerr<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__<<" got signal "<<signal<<std::endl;
  hltRec->stop();
}

void childTerminated(int signal) {
  (void) signal;
  std::cerr<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__
	   <<" Child "<<getpid()<<" terminated with signal "<<signal <<std::endl;
}

void sahandler(int sig,siginfo_t * si,void* /*vp*/){
  std::cerr<<HLTMP::getTimeTag()<<"Got signal "<<sig<<std::endl;
  if(!si){
    std::cerr<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__<<" siginfo_t is NULL "<<std::endl;
    return;
  }
  if(sig==SIGTERM){
    hltRec->stop();
  }
  std::cerr<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__<<"signo="<<si->si_signo
	   <<"  , errno="<<si->si_errno<<std::endl
	   // <<"  , trapno="<<si->si_trapno<<std::endl
    	   <<"  , pid="<<si->si_pid<<std::endl
	   <<"  , uid="<<si->si_uid<<std::endl
	   <<"  , status="<<si->si_status<<std::endl;
  
  std::cerr<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__<<" si_code is =";
  switch (si->si_code){
  case CLD_EXITED:
    std::cerr<<"CLD_EXITED"<<std::endl;
    break;
  case CLD_KILLED:
    std::cerr<<"CLD_KILLED"<<std::endl;
    break;
  case CLD_DUMPED:
    std::cerr<<"CLD_DUMPED"<<std::endl;
    break;
  case CLD_TRAPPED:
    std::cerr<<"CLD_TRAPPED"<<std::endl;
    break;
  case CLD_STOPPED:
    std::cerr<<"CLD_STOPPED"<<std::endl;
    break;
  case CLD_CONTINUED:
    std::cerr<<"CLD_CONTINUED"<<std::endl;
    break;
  default:
    std::cerr<<"OTHER CODE = "<<si->si_code<<std::endl;
    break;
  }
}

namespace po = boost::program_options;

int main(int argc, char * argv[]){
  std::string hltdll;
  try{
    IPCCore::init(argc,argv);
  }catch(std::exception &ex){
    std::cerr<<HLTMP::getTimeTag()<<"IPC initialization Failed with "<<ex.what()<<std::endl;
  }
  try {
    po::options_description desc("This program is Multi-Processing version of HLT Processing unit.");
    desc.add_options()
      ("dllName,d"         , po::value<std::string>(&hltdll)->default_value("libHLTMPPU.so") , "Name of the dll containing implementation")
      ("help,h"                                                                               , "Print help message")
      ;
    
    std::cout<<HLTMP::getTimeTag()<<"Starting HLTMPPU (built on  "<<__DATE__<<" " << __TIME__<<" )"<<std::endl;
    std::cout<<HLTMP::getTimeTag()<<"Input parameters "<<std::endl;
    for(int i=0;i<argc;i++){
      std::cout<<" "<<i<<" \""<<argv[i]<<"\""<<std::endl;
    }
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
    po::notify(vm);
    std::cout<<HLTMP::getTimeTag()<<"Got input parameters "<<std::endl;
    po::basic_parsed_options<char> bop=po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
    for(size_t t=0;t<bop.options.size();t++){
      for(size_t k=0;k<bop.options.at(t).original_tokens.size();k++){
	std::cout<<" "<<bop.options.at(t).string_key<<" ["<<k<<"] = "
		 <<bop.options.at(t).original_tokens.at(k)
		 <<((bop.options.at(t).unregistered)?" (IGNORED) ":"")
		 <<std::endl;
      }
    }
    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }
    if(hltdll.length()==0) {
      ers::error(HLTMPPUIssue::CommandLineIssue(ERS_HERE,"Missing dll name!"));
      std::cout << desc << std::endl;
      return EXIT_FAILURE;
    }
    std::cout<<HLTMP::getTimeTag()<<"Dumping environment"<<std::endl;
    for (char **env=environ; *env!=0;env++){
      std::cout<<*env<<std::endl;
    }
  }catch(std::exception& ex) {
    ers::error(HLTMPPUIssue::CommandLineIssue(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }
  struct sigaction act;
  memset (&act, '\0', sizeof(act));
  act.sa_sigaction=&sahandler;
  act.sa_flags=SA_SIGINFO;
  if(sigaction(SIGTERM,&act,NULL)<0){
   std::cerr<<HLTMP::getTimeTag()<<"Error setting signal handler for SIGTERM"<<std::endl;
  }
  void * myDllHandle=dlopen(hltdll.c_str(),RTLD_LAZY|RTLD_GLOBAL);
  if(!myDllHandle){
    const char* errmsg=dlerror();
    if(errmsg){
      ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,(std::string("can't find ")+hltdll+" "+std::string(errmsg)).c_str()));
    }else{
      ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,(std::string("can't find ")+hltdll).c_str()));
    }
    return EXIT_FAILURE;
  }

  using creator_dlsym = creator (*)(void *, const char*);
  creator c=reinterpret_cast<creator_dlsym>(dlsym)(myDllHandle,"create_interface");
  const char* dlsymError=dlerror();
  if(dlsymError){
    ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,"can't import create_interface function!"));
    return EXIT_FAILURE;
    
  }
  using destroyer_dlsym = destroyer (*)(void *, const char*);
  destroyer d=reinterpret_cast<destroyer_dlsym>(dlsym)(myDllHandle,"destroy_interface");
  dlsymError=dlerror();
  if(dlsymError){
    ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,"can't import destroy_interface function!"));
    return EXIT_FAILURE;
  }
  hltinterface::HLTInterface *i=c();
  try {
    boost::shared_ptr<hltinterface::HLTInterface> hltInt(i,std::ptr_fun(d));
    hltRec.reset(new daq::rc::hlt::HLTReceiver(hltInt));
    
    std::cout<<HLTMP::getTimeTag() <<__PRETTY_FUNCTION__<< "---> Starting the main loop to receive commands..."<<std::endl;
    ERS_LOG("Waiting for hlt commands");
    hltRec->start();

    ERS_LOG("Receiver Returned. Main Exiting!");
    std::cout<<__PRETTY_FUNCTION__<<" main is exiting "<<std::endl;
    return EXIT_SUCCESS;
  }catch(daq::rc::hlt::HLTReceiverError& ex) {
    std::cerr<<HLTMP::getTimeTag()<<"Got a hltreceiver error"<<std::endl;
    ers::fatal(ex);
    return EXIT_FAILURE;
  }catch(std::exception &ex){
    std::cerr<<HLTMP::getTimeTag()<<"Got an exception"<<ex.what()<<std::endl;
    char buff[800];
    snprintf(buff,800,"Caught an unexpected exception \"%s\" exiting!",ex.what());
    ers::warning(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,buff,ex));
    return EXIT_FAILURE;
  }
  std::cout<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__<<" main() is exiting "<<std::endl;
  return 0;
}
