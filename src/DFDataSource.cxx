#include "HLTMPPU/DFDataSource.h"
#include "dfinterface/dfinterface.h"
#include "eformat/eformat.h"
#include "eformat/write/ROBFragment.h"
#include "ers/ers.h"
#include "HLTMPPU/HLTMPPU_utils.h"
#include "hltinterface/IInfoRegister.h"

#include "TH1F.h"
#include "TH2F.h"

#include <boost/foreach.hpp>
#include <boost/dll.hpp>

#include <functional>

HLTMP::DFDataSource::~DFDataSource(){
  ERS_DEBUG(1, "Destructing DFDataSource");
}

HLTMP::DFDataSource::DFDataSource(){
  ERS_LOG("Loading DFDataSource (built on "<<__DATE__<<" "<<__TIME__<<")");
  DataCollector::instance(this);
}

bool HLTMP::DFDataSource::configure(const boost::property_tree::ptree &pt){
  m_configTree=pt;
  m_slots = pt.get("Configuration.HLTMPPUApplication.numberOfEventSlots",4);
  m_getNextTimeout = pt.get<uint16_t>("Configuration.HLTMPPUApplication.getNextTimeout");
  m_dfinterface_library_name = pt.get<std::string>("Configuration.HLTMPPUApplication.DataSourceLibrary");

  // Initialize ROSLatency instance
  m_ros_latency = std::make_unique<ROSLatency>(pt);

  return true;
}

bool HLTMP::DFDataSource::prepareForRun(const boost::property_tree::ptree & /*args*/){

  return true;
}

std::string HLTMP::DFDataSource::globalIDsAsString() {
  std::string result;
  std::shared_lock lck(m_collectMutex);
  for (auto &event: m_events) {
    if(result.empty()){
      result = std::to_string(event.first);
    } else {
      result = result + ", " + std::to_string(event.first);
    }
  }

  return result;
}

bool HLTMP::DFDataSource::wrongGid(uint64_t global_id) {
  std::shared_lock lck(m_collectMutex);
  return (m_events.find(global_id) == m_events.end());
}

void HLTMP::ROSLatency::fillRosRequestStats(std::vector<hltinterface::DCM_ROBInfo>& data) {
  std::map<std::string, uint32_t> ros_size; // ros vs total size of ROBS in bytes
  std::map<std::string, uint32_t> ros_duration; // duration(in microseconds) to get robs
  for (auto & robinfo : data) {
    ERS_DEBUG(5, "ROB: " << robinfo.robFragment.rob_source_id() << ", size(bytes): " << robinfo.robFragment.rod_fragment_size_word()*4 << ", cached: " << robinfo.robIsCached);
    if (m_rob2ros.find(robinfo.robFragment.rob_source_id()) == m_rob2ros.end()) {  // Bad ROB ID
      ERS_DEBUG(5, "ROB not in ROS2ROB map, no ROS histogram will be filled for this ROB");
      continue;
    }
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(robinfo.robDeliveryTime - robinfo.robRequestTime);
    uint32_t size = robinfo.robFragment.rod_fragment_size_word()*4;
    uint32_t duration_us = duration.count();
    auto ros = m_rob2ros[robinfo.robFragment.rob_source_id()];
    ros_size[ros] += size;  // Create or add to element
    ros_duration[ros] += duration_us;  // Create or add to element
  }

  for (auto & [ros, size] : ros_size) {
    auto duration = ros_duration[ros];
    m_ros_histos[ros]->Fill(size, duration);
    if (size > m_max_size_bytes || duration > m_max_duration_us) {
      ERS_LOG("Data exceeds histogram limits, size: " << size << " (limit: " << m_max_size_bytes << ") "
              << "duration: " << duration << " (limit: " << m_max_duration_us << ")");
    }
    ERS_DEBUG(4, "ROS: " << ros << " size(bytes): " << size << ", duration(us): " << ros_duration[ros]);
  }
}

uint32_t HLTMP::DFDataSource::collect(
 std::vector<hltinterface::DCM_ROBInfo>& data,
 const uint64_t global_id, const std::vector<uint32_t>& ids){
  ERS_DEBUG(3,"gid: " << global_id << " called collect");
  if (wrongGid(global_id)) {
    ERS_LOG("Called collect with wrong gid. Current global ids="<< globalIDsAsString()  <<" asked="<< global_id);
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "Wrong gid");
  }else{
    try{
      std::shared_lock lck(m_collectMutex);
      m_events[global_id]->getRobs(ids,data);
      if (m_ros_latency->isActive()) {
        m_ros_latency->fillRosRequestStats(data);
      }
    }catch (daq::dfinterface::CommunicationError &ex){
      ERS_LOG("gid: " << global_id << " Communication error in dfinterface: " << ex.what());
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
    }catch (std::exception &ex){
      ERS_LOG("gid: " << global_id << " DFDataSource Caught exception: \""<<ex.what()<<"\" throwing!");
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
    }
  }
  ERS_DEBUG(3,"gid: " << global_id << " End collect");
  return 0;
}

uint32_t HLTMP::DFDataSource::collect(
std::vector<hltinterface::DCM_ROBInfo>& data,
 uint64_t global_id){
  ERS_DEBUG(3,"gid: " << global_id << " called collect all");
  if (wrongGid(global_id)){
    ERS_LOG("Called collect with wrong gid. Current global ids="<< globalIDsAsString()  <<" asked="<< global_id);
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "Wrong gid");
  }else{
    try{
      std::shared_lock lck(m_collectMutex);
      m_events[global_id]->getAllRobs(data);
      if (m_ros_latency->isActive()) {
        m_ros_latency->fillRosRequestStats(data);
      }
    }catch (daq::dfinterface::CommunicationError &ex){
      ERS_LOG("gid: " << global_id << " Communication error in dfinterface: " << ex.what());
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
    }catch (std::exception &ex){
      ERS_LOG("gid: " << global_id << " DFDataSource Caught exception: \""<<ex.what()<<"\" throwing!");
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
    }
  }
  ERS_DEBUG(3,"gid: " << global_id << " End collect all");
  return 0;
}

void HLTMP::DFDataSource::reserveROBData(const uint64_t global_id, 
					 const std::vector<uint32_t>& ids){
  ERS_DEBUG(3,"gid: " << global_id << " called reserveROBData");
  if (wrongGid(global_id)){
    ERS_LOG("Called with wrong gid. Current global ids="<< globalIDsAsString()  <<" asked="<< global_id);
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "Wrong gid");
  }else{
    try{
      std::shared_lock lck(m_collectMutex);
      m_events[global_id]->mayGetRobs(ids);
    }catch (daq::dfinterface::CommunicationError &ex){
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
    }catch(std::exception &ex){
      ERS_LOG("gid: " << global_id << " Caught exception \" "<<ex.what()<<" \" ");
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
    }
  }
  ERS_DEBUG(3,"gid: " << global_id << " End reserveROBData");
}

// Dcm expects each session to call getNext after STOP is called. This way it will
// close the sessions properly. If we don't run finalizeSession, DCM will think that the
// PU has crashed.
bool HLTMP::DFDataSource::finalizeSession(const size_t session_id){
  try {
    ERS_DEBUG(2, "session: " << session_id << " called getNext to clean dcm connection");
    ERS_DEBUG(3,"Calling tryGetNextUntil with timeout of " << m_getNextTimeout << "ms");
    std::unique_ptr<daq::dfinterface::Event>event = m_sessions[session_id]->tryGetNextUntil(std::chrono::steady_clock::now() + std::chrono::milliseconds(m_getNextTimeout));
  }
    catch (daq::dfinterface::NoMoreEvents &ex) {
    ERS_DEBUG(2, "session: " << session_id << " received NoMoreEvents as expected");
    return true;
  } 
    catch (daq::dfinterface::OperationTimedOut  &ex){
    ERS_LOG("OperationTimedOut received from dfinterface for session id " << session_id << ", DCM didn't reply");
    return false; 
  }
  throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "Get an unexpected exception from dfinterface.");
}

bool HLTMP::DFDataSource::finalize(const boost::property_tree::ptree& /*args*/){
  try {
    // Call following only if nomoreevents==true
    // We don't want to do any new getNext calls, if HLT is being stopped due to an error in HLT
    if (nomoreevents) {
      while (!m_free_sessions.empty()) {
        auto session_id = m_free_sessions.front();
        m_free_sessions.pop();
        if(!finalizeSession(session_id))
          m_free_sessions.push(session_id);
      }
    }

    for (auto & session: m_sessions) {
      if(session){
        session->close();
      }
    }
  } catch (ers::Issue &e) {
    ERS_LOG("Caught Error when closing connections "<<e.what());
    return false;
  } catch (std::exception &e) {
    ERS_LOG("Caught exception when closing connections "<<e.what());
    return false;
  }
  return true;
}

hltinterface::DataCollector::Status HLTMP::DFDataSource::getNext (std::unique_ptr<uint32_t[]>& l1r) {
  std::lock_guard<std::mutex> lck(m_nextMutex);
  std::unique_ptr<daq::dfinterface::Event> event;
  EventTimeInfo temp;
  temp.start = std::chrono::steady_clock::now();
  auto session_id = m_free_sessions.front();
  ERS_DEBUG(2, "Calling getNext(), session_id: "<< session_id);
  try{
    m_free_sessions.pop();
    ERS_DEBUG(3,"Calling tryGetNextUntil with timeout of " << m_getNextTimeout << "ms");
    event=m_sessions[session_id]->tryGetNextUntil(std::chrono::steady_clock::now() + std::chrono::milliseconds(m_getNextTimeout));
  }catch (daq::dfinterface::NoMoreEvents &ex){
    ERS_LOG("NoMoreEvents, returning");
    nomoreevents = true;
    return hltinterface::DataCollector::Status::STOP;
  }catch (daq::dfinterface::CommunicationError &ex){
    ERS_LOG("CommunicationError received from dfinterface, returning NO_EVENT");
    return hltinterface::DataCollector::Status::NO_EVENT;
  }catch (daq::dfinterface::OperationTimedOut  &ex){
    ERS_LOG("OperationTimedOut received from dfinterface, returning NO_EVENT");
    //pushing back the session_id in the m_free_sessions queue
    m_free_sessions.push(session_id);
    return hltinterface::DataCollector::Status::NO_EVENT;
  }catch (std::exception &ex){
    ERS_LOG("DFDataSource Caught exception: \""<<ex.what()<<"\" throwing!");
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
  }

  event->getL1Result(l1r);

  auto gid = event->gid();

  m_event_session[gid] = session_id;
  ERS_DEBUG(2,"Got event with global id : " <<  gid << " session_id: " << session_id);

  if (m_events.find(gid) != m_events.end()){
    ERS_LOG("DUPLICATE EVENT! global id : " <<  gid << " already in the map! This shouldn't have happened");
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "duplicate global id!");
  }
  {  // Protect this part, collect can be called concurrently
    std::unique_lock lck(m_collectMutex);
    m_events[gid] = std::move(event);
  }
  temp.l1 = std::chrono::steady_clock::now();
  m_eventTimes[gid] = temp;
  return hltinterface::DataCollector::Status::OK;
}

void HLTMP::DFDataSource::eventDone (std::unique_ptr<uint32_t[]> hltr) {
  ERS_DEBUG(2, "Enter");
  std::lock_guard<std::mutex> lck(m_nextMutex);
  auto temp = std::chrono::steady_clock::now();
  eformat::read::FullEventFragment ev(hltr.get());

  auto gid = ev.global_id();
  m_eventTimes[gid].proc = temp;
  auto session_id = m_event_session[gid];

  ERS_DEBUG(2,"Event done for global id : " <<  gid << " session_id: " << session_id);

  if (m_events.find(gid) == m_events.end()){
    ERS_LOG("global id : " <<  gid << " not known!");
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "Global id not known");
  }
  bool accept = ((ev.nstream_tag() > 0)?true:false);
  ERS_DEBUG(2,"Number of stream tags: " <<  ev.nstream_tag());

  try{
    if(accept){
      ERS_DEBUG(2,"Event accepted");
      m_sessions[session_id]->accept(std::move(m_events[gid]),
                    std::move(hltr));
      // TODO(cyildiz): In Run-II, there is a case where HLTResult.hltResult_robs.size=0
      // In this case a new ROBFragment is creates using fragment_pointer and passed in the accept call
      // We need to decide what to do in this case...
    }else{
      ERS_DEBUG(2,"Event rejected");
      m_sessions[session_id]->reject(std::move(m_events[gid]));
    }
    m_eventTimes[gid].send = std::chrono::steady_clock::now();
  } catch (daq::dfinterface::CommunicationError &ex) {
    ERS_LOG("Communication error in dfinterface: " << ex.what());
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
  } catch (std::exception &ex) {
    ERS_LOG("DFDataSource Caught exception: \""<<ex.what()<<"\" throwing!");
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
  }

  // After sending results, you can erase current event
  {  // Protect this part, collect can be called concurrently
    std::unique_lock lck(m_collectMutex);
    m_events.erase(gid);
  }
  m_event_session.erase(gid);
  m_free_sessions.emplace(session_id);
  ERS_DEBUG(2, "Results sent to DCM for " << gid << ", global ids of remaining events: " << globalIDsAsString());
  m_eventTimes[gid].end = std::chrono::steady_clock::now();

  auto t = m_eventTimes[gid];
  {
    std::lock_guard<std::mutex> lck(m_statMutex);
    m_currentStats.numEvents++;
    auto wait = std::chrono::duration_cast<std::chrono::milliseconds>(t.l1-t.start);
    m_currentStats.waitDuration+=wait;

    if (m_currentStats.longestWaitForL1Result < wait) {
      m_currentStats.longestWaitForL1Result = wait;
    }

    auto proc=std::chrono::duration_cast<std::chrono::milliseconds>(t.proc-t.l1);
    m_currentStats.processDuration+=proc;
    if (m_currentStats.longestProcessingTime < proc) {
      m_currentStats.longestProcessingTime = proc;
    }

    if (accept) {
      m_currentStats.acceptDuration+=proc;
      m_currentStats.acceptedEvents++;
      m_histos[1]->Fill(proc.count());
    } else {
      m_currentStats.rejectDuration+=proc;
      m_currentStats.rejectedEvents++;
      m_histos[2]->Fill(proc.count());
    }

    auto send=std::chrono::duration_cast<std::chrono::milliseconds>(t.send-t.proc);
    m_currentStats.sendDuration+=send;

    auto total=std::chrono::duration_cast<std::chrono::milliseconds>(t.end-t.start);
    m_currentStats.totalDuration+=total;
    m_histos[0]->Fill(wait.count());
    m_histos[3]->Fill(proc.count());
  }
  m_eventTimes.erase(gid);
}

bool HLTMP::DFDataSource::prepareWorker(const boost::property_tree::ptree& args){
  m_configTree.add_child("ChildParams",args);
  std::string clientName=args.get_child("clientName").data();
  std::string full_libname = "lib" + m_dfinterface_library_name + ".so";
  m_dfinterface_library = boost::dll::shared_library(full_libname, boost::dll::load_mode::type::search_system_folders | boost::dll::load_mode::type::rtld_global);
  using cSession = std::unique_ptr<daq::dfinterface::Session> (const boost::property_tree::ptree&);
  std::function<cSession> cs;
  try {
    cs = m_dfinterface_library.get<cSession>("createSession");
  } catch (std::exception & ex) {
    ERS_LOG("Cant load function createSession. Are you trying to use incorrect library? - " << ex.what());
    throw HLTMPPUIssue::DataSourceConfiguration(ERS_HERE, "Cant load function createSession() from DataSource library. Are you trying to use incorrect library?");
  }
  try {
    for (size_t i = 0; i < m_slots; i++) {  // Create a session for each event slot
      m_sessions.push_back(std::move(cs(m_configTree)));
      std::stringstream ss;
      ss << clientName << "-" << std::setfill('0') << std::setw(2) << i;
      auto name = ss.str();
      m_sessions.back()->open(name);
      m_free_sessions.emplace(i);
    }
  }catch(const daq::dfinterface::BadConfiguration &ex){
    ERS_LOG("DFDataSource failed to create session due to BadConfiguration" << ex.what());
    std::string message = std::string("DFDataSource failed to create session due to bad configuration of dfinterface. Error from dfinterface is: ")
                          + std::string(ex.what());
    throw HLTMPPUIssue::DataSourceConfiguration(ERS_HERE, message.c_str());
  }catch(const daq::dfinterface::CommunicationError &ex){
    ERS_LOG("DFDataSource failed to create session with communication error"<<ex.what());
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
  }catch(const ers::Issue &ex){
    ERS_LOG("Caught ers::Issue \""<<ex.what()<<"\"");
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
  }catch(std::exception &ex){
    ERS_LOG("Caught std::exception \""<<ex.what()<<"\"");
    throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, ex.what());
  }

  for (auto & session : m_sessions) {
    if(!session->isOpen()){
      ERS_LOG("DFDataSource failed to create Session");
      throw HLTMPPUIssue::DataSourceIssue(ERS_HERE, "DFDataSource failed to open a connection to dcm.");
    }
  }

  m_histos.push_back(new TH1F("L1RequestTiming","L1Result receive times",1000,0.,1000.));
  m_histos.push_back(new TH1F("AcceptedTiming","Event Accept Duration",1000,0.,4000.));
  m_histos.push_back(new TH1F("RejectedTiming","Event Reject Duration",1000,0.,4000.));
  m_histos.push_back(new TH1F("ProcessingTiming","Event Processing Duration",1000,0.,4000.));
  if (hltinterface::IInfoRegister::instance()) {  // may not be available in athenaHLT
    hltinterface::IInfoRegister::instance()->registerTObject("HLTMPPU","L1RequestTime",m_histos[0]);
    hltinterface::IInfoRegister::instance()->registerTObject("HLTMPPU","AcceptDecisionTime",m_histos[1]);
    hltinterface::IInfoRegister::instance()->registerTObject("HLTMPPU","RejectDecisionTime",m_histos[2]);
    hltinterface::IInfoRegister::instance()->registerTObject("HLTMPPU","TotalProcessingTime",m_histos[3]);
  }

  if (m_ros_latency->isActive()) {
    m_ros_latency->prepareWorker();
  }

  return true;
}

bool HLTMP::DFDataSource::finalizeWorker(const boost::property_tree::ptree& /*args*/){
  ERS_LOG("Accumulated stats");
  m_accumulatedStats.print();
  m_histos.clear();

  return true;
}

boost::property_tree::ptree HLTMP::DFDataSource::getStatistics() {
  std::lock_guard<std::mutex> lck(m_statMutex);
  m_accumulatedStats.numEvents += m_currentStats.numEvents;
  m_accumulatedStats.acceptedEvents += m_currentStats.acceptedEvents;
  m_accumulatedStats.rejectedEvents += m_currentStats.rejectedEvents;
  m_accumulatedStats.longestWaitForL1Result = std::max(m_accumulatedStats.longestWaitForL1Result, m_currentStats.longestWaitForL1Result);
  m_accumulatedStats.longestProcessingTime  = std::max(m_accumulatedStats.longestProcessingTime, m_currentStats.longestProcessingTime);
  m_accumulatedStats.waitDuration += m_currentStats.waitDuration;
  m_accumulatedStats.sendDuration += m_currentStats.sendDuration;
  m_accumulatedStats.processDuration += m_currentStats.processDuration;
  m_accumulatedStats.acceptDuration += m_currentStats.acceptDuration;
  m_accumulatedStats.rejectDuration += m_currentStats.rejectDuration;
  m_accumulatedStats.totalDuration += m_currentStats.totalDuration;

  boost::property_tree::ptree pt;

  pt.put("numEventsTotal",m_accumulatedStats.numEvents);
  pt.put("acceptedEventsTotal",m_accumulatedStats.acceptedEvents);
  pt.put("rejectedEventsTotal",m_accumulatedStats.rejectedEvents);
  pt.put("numEvents",m_currentStats.numEvents);
  pt.put("acceptedEvents",m_currentStats.acceptedEvents);
  pt.put("rejectedEvents",m_currentStats.rejectedEvents);
  pt.put("longestWaitForL1Result",m_currentStats.longestWaitForL1Result.count());
  pt.put("longestProcessingTime",m_currentStats.longestProcessingTime.count());
  pt.put("waitDuration",m_currentStats.waitDuration.count());
  pt.put("sendDuration",m_currentStats.sendDuration.count());
  pt.put("processDuration",m_currentStats.processDuration.count());
  pt.put("acceptDuration",m_currentStats.acceptDuration.count());
  pt.put("rejectDuration",m_currentStats.rejectDuration.count());
  pt.put("totalDuration",m_currentStats.totalDuration.count());

  m_currentStats.reset();
  return pt;
}

void HLTMP::DFDataSource::Stats::reset() {
  std::chrono::duration<double, std::milli>  ms0 {0.};
  numEvents=0;
  acceptedEvents = 0;
  rejectedEvents = 0;
  longestWaitForL1Result = ms0;
  longestProcessingTime = ms0;
  waitDuration = ms0;
  sendDuration = ms0;
  processDuration = ms0;
  acceptDuration = ms0;
  rejectDuration = ms0;
  totalDuration = ms0;
}

void HLTMP::DFDataSource::Stats::print() {
  double accept=0, reject=0;
  if (acceptedEvents) {
      accept = acceptDuration.count()/acceptedEvents;
  }
  if (rejectedEvents) {
      reject = rejectDuration.count()/rejectedEvents;
  }
  std::cout << "Number of:\n"
            << "  events:     " << numEvents << "\n"
            << "  - accepted: " << acceptedEvents << "\n"
            << "  - rejected: " << rejectedEvents << "\n"
            << "Average durations (ms) / fraction of total time:" << "\n";
  if (numEvents) {
      std::cout << "  l1Result:   " << waitDuration.count()/numEvents << " / " << waitDuration.count()/totalDuration.count() << "\n"
                << "  process:    " << processDuration.count()/numEvents <<  " / " << processDuration.count()/totalDuration.count() << "\n"
                << "  - accept:   " << accept << " / " << acceptDuration.count()/totalDuration.count() <<  "\n"
                << "  - reject:   " << reject << " / " << rejectDuration.count()/totalDuration.count() <<  "\n"
                << "  send:       " << sendDuration.count()/numEvents << " / " << sendDuration.count()/totalDuration.count() <<  "\n"
                << "  total:      " << totalDuration.count()/numEvents << "\n";
  }
  std::cout << "Longest times (ms) : \n"
            << " - L1 result  : " << longestWaitForL1Result.count() << "\n"
            << " - Processing : " << longestProcessingTime.count() << "\n" ;
}

void HLTMP::ROSLatency::prepareWorker() {
  // Get rob2ros mapping, initialize histograms
  using ptree=boost::property_tree::ptree;
  for (const auto &rostree : m_ros2robtree) {
    std::string rosname = rostree.first;
    boost::property_tree::ptree robs = rostree.second;
    m_ros_histos[rosname] = new TH2F(rosname.c_str(), "size(bytes) vs duration(us)", 1000, 0., m_max_size_bytes, 1000, 0., m_max_duration_us);
    std::string hist_path = std::string("EXPERT/ROSLatency/") + rosname;
    if (hltinterface::IInfoRegister::instance()) {
      hltinterface::IInfoRegister::instance()->registerTObject("HLTMPPU",hist_path,m_ros_histos[rosname]);
    }
    for (const auto &rob : robs) {
        uint32_t robid = rob.second.get_value<uint32_t>();
        m_rob2ros[robid] = rosname;
    }
  }
}

HLTMP::ROSLatency::ROSLatency(const boost::property_tree::ptree & pt) {
  if (pt.get_child_optional("Configuration.HLTMPPUApplication.ROSLatencyMeasurement")) {
    ers::warning(HLTMPPUIssue::DataSourceConfiguration(ERS_HERE,"ROSLatencyMeasurement active, this option should NEVER be used in physics runs. If you are running a test, it may be fine"));
    m_active = true;
    m_ros2robtree = pt.get_child("Configuration.ROS2ROBS");
    m_max_size_bytes = pt.get("Configuration.HLTMPPUApplication.ROSLatencyMeasurement.maxSizeBytes",10000);
    m_max_duration_us = pt.get("Configuration.HLTMPPUApplication.ROSLatencyMeasurement.maxDurationMicroSeconds",10000);
    ERS_LOG("ROS latency calculation active, maximum size(bytes): " << m_max_size_bytes << ", maximum duration(microseconds): " << m_max_duration_us);
  } else {
    m_active = false;
    ERS_LOG("ROS latency calculation NOT active");
  }
}
